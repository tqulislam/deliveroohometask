package com.deliveroo.home.task;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.deliveroo.home.task.CronParserUtil;

import junit.framework.TestCase;

public class CronParserDayOfMonthTest extends TestCase {
	private CronParserUtil cronParserUtil = new CronParserUtil();

	@Test
	@DisplayName("Test to check day of month list provided with comma")
	public void testToCheckDayOfMonthProvidedWithComma() throws Exception {
		List<Integer> expectedDayOfMonths = cronParserUtil.extractDayOfMonth("1,12");
		assertEquals(StringUtils.join(expectedDayOfMonths, " "), "1 12");
	}

	@Test
	@DisplayName("Test to check day of month list provided with dash")
	public void testToCheckDayOfMonthProvidedWithDash() throws Exception {
		List<Integer> expectedDayOfMonths = cronParserUtil.extractDayOfMonth("10-12");
		assertEquals(StringUtils.join(expectedDayOfMonths, " "), "10 11 12");
	}

	@Test
	@DisplayName("Test to check day of month list provided with dash")
	public void testToCheckDayOfMonthProvidedWithDayOfMonthsInterval() throws Exception {
		List<Integer> expectedDayOfMonths = cronParserUtil.extractDayOfMonth("*/10");
		assertEquals(StringUtils.join(expectedDayOfMonths, " "), "10 20 30");
	}

	@Test
	@DisplayName("Test to check day of month list provided with dash")
	public void testToCheckAllDayOfMonth() throws Exception {
		List<Integer> actualResult = new ArrayList<Integer>();
		for (int i = CronParserUtil.MINIMUM_DAYS_IN_MONTH; i <= CronParserUtil.MAXIMUM_DAYS_IN_MONTH; i++) {
			actualResult.add(i);
		}
		List<Integer> expectedDayOfMonths = cronParserUtil.extractDayOfMonth("*");
		assertEquals(expectedDayOfMonths, actualResult);
	}

	@Test
	@DisplayName("Test to validate day of month with Star provided with dash")
	public void testToValidateDayOfMonthsWithStar() {
		assertEquals(cronParserUtil.validateDayOfMonthInput("*"), true);
	}

	@Test
	@DisplayName("Test to validate day of month with comma provided with dash")
	public void testToValidateDayOfMonthsWithComma() {
		assertEquals(cronParserUtil.validateDayOfMonthInput("1,2"), true);
	}

	@Test
	@DisplayName("Test to validate day of month with Dash provided with dash")
	public void testToValidateDayOfMonthsWithDash() {
		assertEquals(cronParserUtil.validateDayOfMonthInput("1-2"), true);
	}

	@Test
	@DisplayName("Test to validate day of month with Dash provided with dash")
	public void testToValidateDayOfMonthsWithDividingAll() {
		assertEquals(cronParserUtil.validateDayOfMonthInput("*/10"), true);
	}

	@Test
	@DisplayName("Test to validate day of month with comma provided with dash and invalid number")
	public void testToValidateDayOfMonthsWithCommaAndInvalidNumber() {
		assertEquals(cronParserUtil.validateDayOfMonthInput("1,40"), false);
	}

	@Test
	@DisplayName("Test to validate day of month with Dash provided with dash")
	public void testToValidateDayOfMonthsWithDashAndInvalidNumber() {
		assertEquals(cronParserUtil.validateDayOfMonthInput("0-10"), false);
	}

	@Test
	@DisplayName("Test to validate day of month with Dash provided with dash")
	public void testToValidateDayOfMonthsWithDividingAllAndInvalidNumber() {
		assertEquals(cronParserUtil.validateDayOfMonthInput("*/90"), false);
	}

}
