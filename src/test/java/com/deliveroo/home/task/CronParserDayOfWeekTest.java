package com.deliveroo.home.task;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.deliveroo.home.task.CronParserUtil;

import junit.framework.TestCase;

public class CronParserDayOfWeekTest extends TestCase {
	private CronParserUtil cronParserUtil = new CronParserUtil();

	@Test
	@DisplayName("Test to check day of week list provided with comma")
	public void testToCheckDayOfWeekProvidedWithComma() throws Exception {
		List<Integer> expectedDayOfWeeks = cronParserUtil.extractDayOfWeek("1,12");
		assertEquals(StringUtils.join(expectedDayOfWeeks, " "), "1 12");
	}

	@Test
	@DisplayName("Test to check day of week list provided with dash")
	public void testToCheckDayOfWeekProvidedWithDash() throws Exception {
		List<Integer> expectedDayOfWeeks = cronParserUtil.extractDayOfWeek("10-12");
		assertEquals(StringUtils.join(expectedDayOfWeeks, " "), "10 11 12");
	}

	@Test
	@DisplayName("Test to check day of week list provided with dash")
	public void testToCheckDayOfWeekProvidedWithDayOfWeeksInterval() throws Exception {
		List<Integer> expectedDayOfWeeks = cronParserUtil.extractDayOfWeek("*/2");
		assertEquals(StringUtils.join(expectedDayOfWeeks, " "), "2 4 6");
	}

	@Test
	@DisplayName("Test to check day of week list provided with dash")
	public void testToCheckAllDayOfWeek() throws Exception {
		List<Integer> actualResult = new ArrayList<Integer>();
		for (int i = CronParserUtil.MINIMUM_DAYS_IN_WEEK; i <= CronParserUtil.MAXIMUM_DAYS_IN_WEEK; i++) {
			actualResult.add(i);
		}
		List<Integer> expectedDayOfWeeks = cronParserUtil.extractDayOfWeek("*");
		assertEquals(expectedDayOfWeeks, actualResult);
	}

	@Test
	@DisplayName("Test to validate day of week with Star provided with dash")
	public void testToValidateDayOfWeeksWithStar() {
		assertEquals(cronParserUtil.validateDayOfWeekInput("*"), true);
	}

	@Test
	@DisplayName("Test to validate day of week with comma provided with dash")
	public void testToValidateDayOfWeeksWithComma() {
		assertEquals(cronParserUtil.validateDayOfWeekInput("1,2"), true);
	}

	@Test
	@DisplayName("Test to validate day of week with Dash provided with dash")
	public void testToValidateDayOfWeeksWithDash() {
		assertEquals(cronParserUtil.validateDayOfWeekInput("1-2"), true);
	}

	@Test
	@DisplayName("Test to validate day of week with Dash provided with dash")
	public void testToValidateDayOfWeeksWithDividingAll() {
		assertEquals(cronParserUtil.validateDayOfWeekInput("*/2"), true);
	}

	@Test
	@DisplayName("Test to validate day of week with comma provided with dash and invalid number")
	public void testToValidateDayOfWeeksWithCommaAndInvalidNumber() {
		assertEquals(cronParserUtil.validateDayOfWeekInput("1,40"), false);
	}

	@Test
	@DisplayName("Test to validate day of week with Dash provided with dash")
	public void testToValidateDayOfWeeksWithDashAndInvalidNumber() {
		assertEquals(cronParserUtil.validateDayOfWeekInput("0-10"), false);
	}

	@Test
	@DisplayName("Test to validate day of week with Dash provided with dash")
	public void testToValidateDayOfWeeksWithDividingAllAndInvalidNumber() {
		assertEquals(cronParserUtil.validateDayOfWeekInput("*/90"), false);
	}

}
