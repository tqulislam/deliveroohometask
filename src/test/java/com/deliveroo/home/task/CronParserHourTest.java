package com.deliveroo.home.task;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.deliveroo.home.task.CronParserUtil;

import junit.framework.TestCase;

public class CronParserHourTest extends TestCase {
	private CronParserUtil cronParserUtil = new CronParserUtil();

	@Test
	@DisplayName("Test to check hour list provided with comma")
	public void testToCheckHourProvidedWithComma() throws Exception {
		List<Integer> expectedHours = cronParserUtil.extractHour("1,12");
		assertEquals(StringUtils.join(expectedHours, " "), "1 12");
	}

	@Test
	@DisplayName("Test to check hour list provided with dash")
	public void testToCheckHourProvidedWithDash() throws Exception {
		List<Integer> expectedHours = cronParserUtil.extractHour("10-12");
		assertEquals(StringUtils.join(expectedHours, " "), "10 11 12");
	}

	@Test
	@DisplayName("Test to check hour list provided with dash")
	public void testToCheckHourProvidedWithHoursInterval() throws Exception {
		List<Integer> expectedHours = cronParserUtil.extractHour("*/6");
		assertEquals(StringUtils.join(expectedHours, " "), "0 6 12 18");
	}

	@Test
	@DisplayName("Test to check hour list provided with dash")
	public void testToCheckAllHour() throws Exception {
		List<Integer> actualResult = new ArrayList<Integer>();
		for (int i = CronParserUtil.MINIMUM_HOUR; i <= CronParserUtil.MAXIMUM_HOUR; i++) {
			actualResult.add(i);
		}
		List<Integer> expectedHours = cronParserUtil.extractHour("*");
		assertEquals(expectedHours, actualResult);
	}

	@Test
	@DisplayName("Test to validate hour with Star provided with dash")
	public void testToValidateHoursWithStar() {
		assertEquals(cronParserUtil.validateHourInput("*"), true);
	}

	@Test
	@DisplayName("Test to validate hour with comma provided with dash")
	public void testToValidateHoursWithComma() {
		assertEquals(cronParserUtil.validateHourInput("1,2"), true);
	}

	@Test
	@DisplayName("Test to validate hour with Dash provided with dash")
	public void testToValidateHoursWithDash() {
		assertEquals(cronParserUtil.validateHourInput("1-2"), true);
	}

	@Test
	@DisplayName("Test to validate hour with Dash provided with dash")
	public void testToValidateHoursWithDividingAll() {
		assertEquals(cronParserUtil.validateHourInput("*/2"), true);
	}

	@Test
	@DisplayName("Test to validate hour with comma provided with dash and invalid number")
	public void testToValidateHoursWithCommaAndInvalidNumber() {
		assertEquals(cronParserUtil.validateHourInput("1,40"), false);
	}

	@Test
	@DisplayName("Test to validate hour with Dash provided with dash")
	public void testToValidateHoursWithDashAndInvalidNumber() {
		assertEquals(cronParserUtil.validateHourInput("0-24"), false);
	}

	@Test
	@DisplayName("Test to validate hour with Dash provided with dash")
	public void testToValidateHoursWithDividingAllAndInvalidNumber() {
		assertEquals(cronParserUtil.validateHourInput("*/90"), false);
	}
}
