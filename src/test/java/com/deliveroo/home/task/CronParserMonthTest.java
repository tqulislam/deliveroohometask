package com.deliveroo.home.task;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.deliveroo.home.task.CronParserUtil;

import junit.framework.TestCase;

public class CronParserMonthTest extends TestCase {
	private CronParserUtil cronParserUtil = new CronParserUtil();

	@Test
	@DisplayName("Test to check month list provided with comma")
	public void testToCheckMonthProvidedWithComma() throws Exception {
		List<Integer> expectedMonths = cronParserUtil.extractMonth("1,12");
		assertEquals(StringUtils.join(expectedMonths, " "), "1 12");
	}

	@Test
	@DisplayName("Test to check month list provided with dash")
	public void testToCheckMonthProvidedWithDash() throws Exception {
		List<Integer> expectedMonths = cronParserUtil.extractMonth("10-12");
		assertEquals(StringUtils.join(expectedMonths, " "), "10 11 12");
	}

	@Test
	@DisplayName("Test to check month list provided with dash")
	public void testToCheckMonthProvidedWithMonthsInterval() throws Exception {
		List<Integer> expectedMonths = cronParserUtil.extractMonth("*/4");
		assertEquals(StringUtils.join(expectedMonths, " "), "4 8 12");
	}

	@Test
	@DisplayName("Test to check month list provided with dash")
	public void testToCheckAllMonth() throws Exception {
		List<Integer> actualResult = new ArrayList<Integer>();
		for (int i = CronParserUtil.MINIMUM_MONTH; i <= CronParserUtil.MAXIMUM_MONTH; i++) {
			actualResult.add(i);
		}
		List<Integer> expectedMonths = cronParserUtil.extractMonth("*");
		assertEquals(expectedMonths, actualResult);
	}

	@Test
	@DisplayName("Test to validate month with Star provided with dash")
	public void testToValidateMonthsWithStar() {
		assertEquals(cronParserUtil.validateMonthInput("*"), true);
	}

	@Test
	@DisplayName("Test to validate month with comma provided with dash")
	public void testToValidateMonthsWithComma() {
		assertEquals(cronParserUtil.validateMonthInput("1,2"), true);
	}

	@Test
	@DisplayName("Test to validate month with Dash provided with dash")
	public void testToValidateMonthsWithDash() {
		assertEquals(cronParserUtil.validateMonthInput("1-2"), true);
	}

	@Test
	@DisplayName("Test to validate month with Dash provided with dash")
	public void testToValidateMonthsWithDividingAll() {
		assertEquals(cronParserUtil.validateMonthInput("*/2"), true);
	}

	@Test
	@DisplayName("Test to validate month with comma provided with dash and invalid number")
	public void testToValidateMonthsWithCommaAndInvalidNumber() {
		assertEquals(cronParserUtil.validateMonthInput("1,70"), false);
	}

	@Test
	@DisplayName("Test to validate month with Dash provided with dash")
	public void testToValidateMonthsWithDashAndInvalidNumber() {
		assertEquals(cronParserUtil.validateMonthInput("0-85"), false);
	}

	@Test
	@DisplayName("Test to validate month with Dash provided with dash")
	public void testToValidateMonthsWithDividingAllAndInvalidNumber() {
		assertEquals(cronParserUtil.validateMonthInput("*/90"), false);
	}
}
