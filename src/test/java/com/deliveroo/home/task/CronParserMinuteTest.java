package com.deliveroo.home.task;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.deliveroo.home.task.CronParserUtil;

import junit.framework.TestCase;

public class CronParserMinuteTest extends TestCase {
	private CronParserUtil cronParserUtil = new CronParserUtil();
	@Test
	@DisplayName("Test to check minute list provided with comma")
	public void testToCheckMinuteProvidedWithComma() throws Exception {
		List<Integer> expectedMinutes = cronParserUtil.extractMinutes("1,12");
		assertEquals(StringUtils.join(expectedMinutes, " "), "1 12");
	}

	@Test
	@DisplayName("Test to check minute list provided with dash")
	public void testToCheckMinuteProvidedWithDash() throws Exception {
		List<Integer> expectedMinutes = cronParserUtil.extractMinutes("10-12");
		assertEquals(StringUtils.join(expectedMinutes, " "), "10 11 12");
	}

	@Test
	@DisplayName("Test to check minute list provided with dash")
	public void testToCheckMinuteProvidedWithMinutesInterval() throws Exception {
		List<Integer> expectedMinutes = cronParserUtil.extractMinutes("*/10");
		assertEquals(StringUtils.join(expectedMinutes, " "), "0 10 20 30 40 50");
	}

	@Test
	@DisplayName("Test to check minute list provided with dash")
	public void testToCheckAllMinute() throws Exception {
		List<Integer> actualResult = new ArrayList<Integer>();
		for (int i = CronParserUtil.MINIMUM_MINUTES; i <= CronParserUtil.MAXIMUM_MINUTES; i++) {
			actualResult.add(i);
		}
		List<Integer> expectedMinutes = cronParserUtil.extractMinutes("*");
		assertEquals(expectedMinutes, actualResult);
	}

	@Test
	@DisplayName("Test to validate minute with Star provided with dash")
	public void testToValidateMinutesWithStar() {
		assertEquals(cronParserUtil.validateMinuteInput("*"), true);
	}

	@Test
	@DisplayName("Test to validate minute with comma provided with dash")
	public void testToValidateMinutesWithComma() {
		assertEquals(cronParserUtil.validateMinuteInput("1,2"), true);
	}

	@Test
	@DisplayName("Test to validate minute with Dash provided with dash")
	public void testToValidateMinutesWithDash() {
		assertEquals(cronParserUtil.validateMinuteInput("1-2"), true);
	}

	@Test
	@DisplayName("Test to validate minute with Dash provided with dash")
	public void testToValidateMinutesWithDividingAll() {
		assertEquals(cronParserUtil.validateMinuteInput("*/2"), true);
	}

	@Test
	@DisplayName("Test to validate minute with comma provided with dash and invalid number")
	public void testToValidateMinutesWithCommaAndInvalidNumber() {
		assertEquals(cronParserUtil.validateMinuteInput("1,70"), false);
	}

	@Test
	@DisplayName("Test to validate minute with Dash provided with dash")
	public void testToValidateMinutesWithDashAndInvalidNumber() {
		assertEquals(cronParserUtil.validateMinuteInput("0-85"), false);
	}

	@Test
	@DisplayName("Test to validate minute with Dash provided with dash")
	public void testToValidateMinutesWithDividingAllAndInvalidNumber() {
		assertEquals(cronParserUtil.validateMinuteInput("*/90"), false);
	}
}
