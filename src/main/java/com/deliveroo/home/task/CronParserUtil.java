package com.deliveroo.home.task;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

public class CronParserUtil {

	public static final String MINUTE_REGEX_PATTERN = "^(((1-5][0-9]|[0-9])\\-([1-5][0-9]|[0-9]))|((([1-5][0-9]|[0-9]),(?:([1-5][0-9]|[0-9]),)*(1-5][0-9]|[0-9])))|(\\*\\/([1-5][0-9]|[0-9]))|([1-5][0-9]|[0-9])|(\\*))$";
	public static final String HOUR_REGEX_PATTERN = "^((([1][0-9]|[2][0-3]|[0-9])\\-([1][0-9]|[2][0-3]|[0-9]))|(([1][0-9]|[2][0-3]|[0-9]),(?:[1][0-9]|[2][0-3]|[0-9],)*([1][0-9]|[2][0-3]|[0-9]))|(\\*\\/([1][0-9]|[2][0-3]|[0-9]))|([1][0-9]|[2][0-3]|[0-9])|(\\*))$";
	public static final String DAY_OF_MONTH_REGEX_PATTERN = "^((([1-2][0-9]|[3][0-1]|[1-9])\\-([1-2][0-9]|[3][0-1]|[1-9]))|(([1-2][0-9]|[3][0-1]|[1-9]),(?:([1-2][0-9]|[3][0-1]|[1-9]),)*([1-2][0-9]|[3][0-1]|[1-9]))|(\\*\\/([1-2][0-9]|[3][0-1]|[1-9]))|([1-2][0-9]|[3][0-1]|[1-9])|(\\*))$";
	public static final String MONTH_REGEX_PATTERN = "^((([1][0-2]|[1-9])\\-([1][0-2]|[1-9]))|(([1][0-2]|[1-9]),(?:([1][0-2]|[1-9]),)*([1][0-2]|[1-9]))|(\\*\\/([1][0-2]|[1-9]))|([1][0-2]|[1-9])|(\\*))$";
	public static final String DAY_OF_WEEK_REGEX_PATTERN = "^((([1-7])\\-([1-7]))|(([1-7]{1}),(?:[1-7]{1},)*([0-7]{1}))|(\\*\\/([1-7]))|([1-7])|(\\*))$";
	public static final Integer MINIMUM_HOUR = 0;
	public static final Integer MAXIMUM_HOUR = 23;
	public static final Integer MINIMUM_MINUTES = 0;
	public static final Integer MAXIMUM_MINUTES = 59;
	public static final Integer MINIMUM_DAYS_IN_MONTH = 1;
	public static final Integer MAXIMUM_DAYS_IN_MONTH = 31;
	public static final Integer MINIMUM_DAYS_IN_WEEK = 1;
	public static final Integer MAXIMUM_DAYS_IN_WEEK = 7;
	public static final Integer MINIMUM_MONTH = 1;
	public static final Integer MAXIMUM_MONTH = 12;

	public CronParser parse(String str) throws Exception {

		try {
			validateInput(str);
			CronParser result = new CronParser();
			String[] inputParts = str.split(" ");
			result.setMinute(extractMinutes(inputParts[0]));
			result.setHour(extractHour(inputParts[1]));
			result.setDayOfMonth(extractDayOfMonth(inputParts[2]));
			result.setMonth(extractMonth(inputParts[3]));
			result.setDayOfWeek(extractDayOfWeek(inputParts[4]));
			result.setCommand(inputParts[5]);
			return result;
		} finally {
		}
	}

	public boolean validateInput(String input) throws Exception {
		if (StringUtils.isBlank(input)) { 
			throw new Exception("Empty input passed");
		}
		String[] inputParts = input.split(" ");
		if (!validateMinuteInput(inputParts[0])) {
			throw new Exception("Invalid minute format");
		} else if (!validateHourInput(inputParts[1])) {
			throw new Exception("Invalid hour format");
		} else if (!validateDayOfMonthInput(inputParts[2])) {
			throw new Exception("Invalid day of month format");
		} else if (!validateMonthInput(inputParts[3])) {
			throw new Exception("Invalid month format");
		} else if (!validateDayOfWeekInput(inputParts[4])) {
			throw new Exception("Invalid day of week format");
		}
		return true;
	}

	public boolean validateMinuteInput(String input) {
		Pattern r = Pattern.compile(MINUTE_REGEX_PATTERN);
		Matcher m = r.matcher(input);
		return m.find();
	}

	public boolean validateHourInput(String input) {
		Pattern r = Pattern.compile(HOUR_REGEX_PATTERN);
		Matcher m = r.matcher(input);
		return m.find();
	}

	public boolean validateDayOfMonthInput(String input) {
		Pattern r = Pattern.compile(DAY_OF_MONTH_REGEX_PATTERN);
		Matcher m = r.matcher(input);
		return m.find();
	}

	public boolean validateMonthInput(String input) {
		Pattern r = Pattern.compile(MONTH_REGEX_PATTERN);
		Matcher m = r.matcher(input);
		return m.find();
	}

	public boolean validateDayOfWeekInput(String input) {
		Pattern r = Pattern.compile(DAY_OF_MONTH_REGEX_PATTERN);
		Matcher m = r.matcher(input);
		return m.find();
	}

	public List<Integer> extractMinutes(String input) throws Exception {
		return extract(input, MINIMUM_MINUTES, MAXIMUM_MINUTES);
	}

	public List<Integer> extractHour(String input) throws Exception {
		return extract(input, MINIMUM_HOUR, MAXIMUM_HOUR);
	}

	public List<Integer> extractDayOfMonth(String input) throws Exception {
		return extract(input, MINIMUM_DAYS_IN_MONTH, MAXIMUM_DAYS_IN_MONTH);
	}

	public List<Integer> extractMonth(String input) throws Exception {
		return extract(input, MINIMUM_MONTH, MAXIMUM_MONTH);
	}

	public List<Integer> extractDayOfWeek(String input) throws Exception {
		return extract(input, MINIMUM_DAYS_IN_WEEK, MAXIMUM_DAYS_IN_WEEK);
	}

	private List<Integer> extract(String input, Integer minimum, Integer maxium) throws Exception {
		List<Integer> result = new ArrayList<Integer>();
		if (input.equalsIgnoreCase("*")) {
			for (int i = minimum; i <= maxium; i++) {
				result.add(i);
			}
		} else if (input.startsWith("*/")) {
			Integer number = Integer.parseInt(input.substring(2));
			if (number < minimum || number > maxium) {
				throw new Exception("Provided input number is not in range");
			}
			for (int i = minimum; i <= maxium; i++) {
				if (i % number == 0) {
					result.add(i);
				}
			}
		} else if (input.contains("-")) {
			String[] parts = input.split("-");
			Integer start = Integer.parseInt(parts[0]);
			Integer end = Integer.parseInt(parts[1]);
			for (int i = start; i <= end; i++) {
				result.add(i);
			}
		} else if (input.contains(",")) {
			String[] parts = input.split(",");
			for (String part : parts) {
				result.add(Integer.parseInt(part));
			}
		} else {
			result.add(Integer.parseInt(input));
		}
		return result;
	}
}
