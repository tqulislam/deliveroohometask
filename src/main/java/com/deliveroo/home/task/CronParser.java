package com.deliveroo.home.task;

import java.util.List;
import org.apache.commons.lang3.StringUtils;

public class CronParser {

	private List<Integer> minute;
	private List<Integer> hour;
	private List<Integer> dayOfMonth;
	private List<Integer> month;
	private List<Integer> dayOfWeek;
	private String command;

	public CronParser() {
	}

	public CronParser(List<Integer> minute, List<Integer> hour, List<Integer> dayOfMonth, List<Integer> month,
			List<Integer> dayOfWeek, String command) {
		super();
		this.minute = minute;
		this.hour = hour;
		this.dayOfMonth = dayOfMonth;
		this.month = month;
		this.dayOfWeek = dayOfWeek;
		this.command = command;
	}

	public List<Integer> getMinute() {
		return minute;
	}

	public void setMinute(List<Integer> minute) {
		this.minute = minute;
	}

	public List<Integer> getHour() {
		return hour;
	}

	public void setHour(List<Integer> hour) {
		this.hour = hour;
	}

	public List<Integer> getDayOfMonth() {
		return dayOfMonth;
	}

	public void setDayOfMonth(List<Integer> dayOfMonth) {
		this.dayOfMonth = dayOfMonth;
	}

	public List<Integer> getMonth() {
		return month;
	}

	public void setMonth(List<Integer> month) {
		this.month = month;
	}

	public List<Integer> getDayOfWeek() {
		return dayOfWeek;
	}

	public void setDayOfWeek(List<Integer> dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	@Override
	public String toString() {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("minute             ");
		stringBuilder.append(StringUtils.join(this.getMinute(), " "));
		stringBuilder.append("\n");

		stringBuilder.append("hour               ");
		stringBuilder.append(StringUtils.join(this.getHour(), " "));
		stringBuilder.append("\n");

		stringBuilder.append("day of month       ");
		stringBuilder.append(StringUtils.join(this.getDayOfMonth(), " "));
		stringBuilder.append("\n");

		stringBuilder.append("month              ");
		stringBuilder.append(StringUtils.join(this.getMonth(), " "));
		stringBuilder.append("\n");

		stringBuilder.append("day of week        ");
		stringBuilder.append(StringUtils.join(this.getDayOfWeek(), " "));
		stringBuilder.append("\n");

		stringBuilder.append("command            ");
		stringBuilder.append(this.getCommand());
		return stringBuilder.toString();
	}

}
