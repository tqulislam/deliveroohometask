package com.deliveroo.home.task;

public class Application {

	public static void main(String[] args) {
		CronParserUtil cronParserUtil = new CronParserUtil();
		try {
			System.out.println(cronParserUtil.parse(args[0]).toString());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
